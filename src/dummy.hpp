class dummy
{
public:
    dummy();

public: // oparator
    virtual ~dummy() = default; // destructor
    dummy(const dummy&) = delete; // copy constractor
    dummy(dummy&&) = delete; // move constractor
    dummy &operator=(const dummy&) = delete; // copy assignment operator
    dummy &operator=(dummy&&) = delete; // move assignment operator
}; // class dummy
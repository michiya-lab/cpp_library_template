#!/bin/bash
rm -f CMakeCache.txt

cmake \
-D CMAKE_CXX_COMPILER=g++ \
-D CMAKE_BUILD_TYPE:STRING=Release \
-D BUILD_TESTING=ON \
-D BUILD_EXAMPLES=ON \
-D CMAKE_INSTALL_PREFIX="./lib" \
-D CMAKE_CXX_FLAGS:STRING="-std=gnu++17 -Wall -Wextra -Wpedantic -Werror" \
-D CMAKE_CXX_FLAGS_RELEASE:STRING="-O2 -DNDEBUG -march=native -mtune=native" \
-D CMAKE_VERBOSE_MAKEFILE=ON \
../
